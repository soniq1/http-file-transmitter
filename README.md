# HTTP File Transmitter

## NOTE: This repo has been moved. 

You can find the latest version here: https://github.com/manny89/HTTP-File-Transmitter

# Previous Summary  
HTTP File Transmitter allows a local directory to be monitored, transmitting files to a folder on a remote system for application consumption. 
The local system runs the `client_transmitter` and the remote system runs the `server_receiver`. 
After transmission, files on the local system are deleted by the `client_transmitter`. 


While not strictly necesssary, it is assumed that after files have been delivered to and processed on the remote system, 
that those files will also be deleted by the consumer application (the remote server does not delete files as it 
has no way of knowing when to do so).
